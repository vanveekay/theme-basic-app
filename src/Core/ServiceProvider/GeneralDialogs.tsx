import * as React from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';



export default class GeneralDialog extends React.Component<any, any> {
    constructor(props: any){
        super(props);
        this.state={
            isOpen: false,
        }
    }

    private handleClose = () => {
        this.setState({isOpen: false});
    }

    render(){
        return(
            <Dialog
                onClose={this.handleClose}
                aria-labelledby="customized-dialog-title"
                open={this.state.isOpen}
                 >
                    <DialogTitle 
                        style={{backgroundColor: "#222", color: "white"}}
                        id="customized-dialog-title"
                    >
                        Order Details
                    </DialogTitle>
                    <DialogContent
                        style={{backgroundColor: "#222", color: "white"}}
                        dividers
                    >

                    <Grid container style={{border: "1px solid lightgrey", borderRadius: "6px",  marginTop: "6px"}} spacing={3}>
                            
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                OrderId:
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.orderId}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Status:
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.status}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid container style={{border: "1px solid lightgrey", borderRadius: "6px", marginTop: "20px"}} spacing={3}>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Stock:
                            </Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <Typography gutterBottom>
                                {this.state.symbol} {this.state.description}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Buy/Sell:
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.side}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography >
                                Order Type
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography >
                                {this.state.orderType}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid container style={{border: "1px solid lightgrey", borderRadius: "6px", marginTop: "20px"}} spacing={3}>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Price: USD$
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.price}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Quantity
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.quantity}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Value: USD$
                            </Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <Typography gutterBottom>
                                {this.state.price * this.state.quantity}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid container style={{border: "1px solid lightgrey", borderRadius: "6px", marginTop: "20px", marginBottom: "10px"}} spacing={3}>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Book/Account:
                            </Typography>
                        </Grid>
                        <Grid item xs={9}>
                            <Typography gutterBottom>
                                {this.state.bookName} | {this.state.bookId}  
                            </Typography>
                        </Grid> 
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Trader:
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.traderName}
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                Trader Id:
                            </Typography>
                        </Grid>
                        <Grid item xs={3}>
                            <Typography gutterBottom>
                                {this.state.traderId}
                            </Typography>
                        </Grid>
                    </Grid>

                    </DialogContent>
                    <DialogActions
                        style={{backgroundColor: "#222", color: "white"}}
                    >
                        <Button onClick={this.handleClose} color="primary">
                            Save changes
                        </Button>
                    </DialogActions>
                </Dialog>
        )
    }
}