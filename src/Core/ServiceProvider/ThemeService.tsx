import * as React from "react";
import {midnightMuiTheme, articMuiTheme, crystalMuiTheme, steelMuiTheme, IMuiTheme} from "./themes/muitheme";
import { MuiThemeProvider} from "@material-ui/core";
import ThemePalatte from "./themes/themePalatte.json";

export interface IThemeService {
    defaultMuiTheme?: IMuiTheme,
    defaultAgTheme?: string,
    setTheme?: (themeStr: string) => void,
    getTheme?: () => void,
    getAgTheme?: ()=> string
}

export enum ThemeNames  {
    MidnightBlack = "MidnightBlack",
    SteelBlue = "SteelBlue",
    CrystalBlue = "CrystalBlue",
    ArticWhite = "ArticWhite"
}

export class ThemeService extends React.Component<any, any> implements IThemeService{
    defaultMuiTheme: IMuiTheme = midnightMuiTheme;
    defaultAgTheme: string = "ag-theme-dark";
    constructor(props: any) {
        super(props)
        this.state={
            currentMuiTheme: this.defaultMuiTheme,
            currentAgTheme: this.defaultAgTheme
        }
    }

    setTheme = (themeStr: string): void => {
        switch(themeStr) {
            case ThemeNames.MidnightBlack:
                this.setState({
                    currentMuiTheme: midnightMuiTheme,
                    currentAgTheme: "ag-theme-dark"
                })
                break;
            case ThemeNames.ArticWhite:
                this.setState({
                    currentMuiTheme: articMuiTheme,
                    currentAgTheme: "ag-theme-fresh"
                })
                break;
            case ThemeNames.CrystalBlue:
                this.setState({
                    currentMuiTheme: crystalMuiTheme,
                    currentAgTheme: "ag-theme-balham"
                })
                break;
            default:
                this.setState({
                    currentMuiTheme: steelMuiTheme,
                    currentAgTheme: "ag-theme-fresh"
                })
                break;
 
        }
    }

    getTheme = () => {
        alert(this.state.currentMuiTheme.themeId);
    }

    getAgTheme = (): string => {
        return this.state.currentAgTheme
    }

    setLayoutTheme = (themeStr: string) => {
        var theme: any = ThemePalatte[themeStr]

        let root = document.documentElement;
        root.style.setProperty('--layout-primary1', theme.Primary1);
        root.style.setProperty('--layout-primary2', theme.Primary2);
        root.style.setProperty('--layout-secondary1', theme.Secondary1);
        root.style.setProperty('--layout-secondary2', theme.Secondary2);
        root.style.setProperty('-layout-contrast1', theme.Contrast1);
        root.style.setProperty('--layout-contrast2', theme.Contrast2);
    }

    render() {
        return (
            <MuiThemeProvider theme={this.state.currentMuiTheme}>
                {this.props.children}
            </MuiThemeProvider>
        )
    }
}
