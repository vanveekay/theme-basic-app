import {Dialog, DialogActions, DialogContent, DialogTitle } from "@material-ui/core";
import * as React from "react";

export interface IDialogServiceContext {
    open: boolean;
    title?: string;
    content?: any;
    actions?: any;
}

export interface IDialogService {
    showDialog: (context: IDialogServiceContext) => void;
}

export class DialogService extends React.Component<any, IDialogServiceContext> implements IDialogService {
    public static getDerivedStateFromProps (props: any, state: IDialogServiceContext) {
        if (state.open === false) {
            return {
                open: false
             }
        } else if (state.open === true) {
            return {
                open: true
            }
        }
        return {open: false}
    }

    public Content = this.props.content;
    public constructor(props: any) {
        super(props);
        this.state = {
            actions: "",
            content: "",
            open: false,
            title: ""
        };
    }

    public render() {
        return (
            <Dialog open={this.state.open} onClose={this.handleClose} >
                <DialogTitle>
                    {this.state.title ? this.state.title : ""}
                </DialogTitle>
                <DialogContent>
                    {this.state.content}
                </DialogContent>
                <DialogActions>
                    {this.state.actions}
                </DialogActions>
            </Dialog>
        )
    }

    public showDialog = (context: IDialogServiceContext): void => {
        this.setState({title: context.title, content: context.content, actions: context.actions, open: context.open})
    }

    private handleClose = () => {
        this.setState({open: false})
    }
}

