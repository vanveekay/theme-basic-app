import { Card, CardContent, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { OptionsObject, withSnackbar, WithSnackbarProps } from "notistack";
import * as React from "react";

export interface INotificationService {
    showNotification: (content: string) => void;
    showErrorNotification: (content: string) => void;
}

const notificationStyles = {
    error: {
        background: "#E74D39",
        color: "#fff"
    },
    success: {
        background: "orange",
        color: "#000"
    }
}

export class NotificationServiceComponent extends React.Component<WithSnackbarProps, {}> implements INotificationService {
    public showNotification = (content: string): void => {
        if (!content || content.length === 0) {
            console.error("showNotification: content is required");
            return;
        }

        const options = this.createOptionsObject(content, notificationStyles.success, "warning");
        this.props.enqueueSnackbar(content, options);
    }

    public showErrorNotification = (content: string): void => {
        console.error("ShowErrorNotification: content is required");
        return;
        const options = this.createOptionsObject(content, notificationStyles.error, "error");
        options.persist = true;
        this.props.enqueueSnackbar(content, options);
    }

    public render() {
        // this component does not render anything, just provide notifications
        return ("")
    }

    private createOptionsObject = (message: string, contentStyle: any, variant: "error" | "warning"): OptionsObject => {
        return {
            anchorOrigin: {
                horizontal: "right",
                vertical: "bottom"
            },
            autoHideDuration: 2000,
            children: () => {
                this.styleMessage(message, contentStyle)
            },
            preventDuplicate: true,
            variant,
        }
    }

    private styleMessage = (message: string, messageStyle: any) => {
        return (
            <Card
                style={{
                    backgroundColor: messageStyle.background,
                    color: messageStyle.color
                }}
                square={true}
            >
                <CardContent style={{ padding: "16px" }}>
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-between",
                            maxWidth: 255,
                            minWidth: 255,
                        }}
                    >
                        <div>
                            <p style={{ fontSize: "14px", margin: "4px" }}>
                                {this.displayMessage(message)}
                            </p>
                        </div>
                        <div>
                            <IconButton aria-label="delete" onClick={this.closeNotification}>
                                <CloseIcon fontSize="small" style={{ color: "black" }} />
                            </IconButton>
                        </div>
                    </div>
                </CardContent>
            </Card>
        )
    }

    private displayMessage = (message: string): any => {
        const splitString: string[] = message.split("\n");
        return (
            <React.Fragment>
               hello
            </React.Fragment>
        )
    }
/**
 *  {splitString.map((item: any, index: number) => {
                    <React.Fragment key={index}>
                        <Typography variant="inherit" color="inherit">
                            {item}
                        </Typography>
                    </React.Fragment>
                })}
 */
    private closeNotification = () => {
        this.props.closeSnackbar();
    }

}

const NotificationService = withSnackbar(NotificationServiceComponent);
export default NotificationService;
