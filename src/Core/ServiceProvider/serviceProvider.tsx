import {CssBaseline, MuiThemeProvider} from "@material-ui/core";
import {SnackbarProvider} from "notistack";
import * as React from "react";
import { DialogService, IDialogService} from "./DialogService";
import NotificationService, {INotificationService, NotificationServiceComponent} from "./NotificationServiceComponent";
import StoreService from "./StoreService";
import {ThemeService, IThemeService} from "./ThemeService";

export const ServiceContext = React.createContext<IServiceProvider>({
    dialogService: undefined,
    notificationService: undefined,
    storeService: undefined,
    svcProvider: undefined,
    themeService: undefined,
});

const ContextProvider = ServiceContext.Provider;

export interface IServiceProvider{
    dialogService?: IDialogService;
    notificationService?: INotificationService;
    storeService?: StoreService;
    svcProvider?: ServiceProvider;
    themeService?: IThemeService;
}

export interface IServiceProviderProps {
   
}

export class ServiceProvider extends React.Component<IServiceProviderProps, any> {
    private dialogService: React.RefObject<DialogService>;
    private notificationService: React.RefObject<NotificationServiceComponent>;
    private storeService: React.RefObject<StoreService>;
    private themeService: React.RefObject<ThemeService>;

    private serviceProvider: IServiceProvider;

    constructor(props: IServiceProviderProps) {
        super(props);
        this.dialogService = React.createRef();
        this.notificationService = React.createRef();
        this.storeService = React.createRef();
        this.themeService = React.createRef();
        this.serviceProvider = {};
        //this.theme = props.theme || lightMuiTheme;
        console.log('serviceprovider...')
        this.state = {
        }
    }

    public render() {
        console.log('serviceprovider... ewnder')
        return (
            <ContextProvider value={this.serviceProvider}>
                <ThemeService ref={this.themeService}>
                    <React.Fragment>
                        <CssBaseline />
                        {this.props.children}
                        <DialogService ref={this.dialogService} />
                        <StoreService ref={this.storeService}/>
                        <SnackbarProvider
                            maxSnack={2}
                            preventDuplicate={true}
                            disableWindowBlurListener={true}
                            hideIconVariant={true}
                        >
                            <NotificationService ref={this.notificationService} />
                        </SnackbarProvider>
                    </React.Fragment>
                </ThemeService>
            </ContextProvider>
        )
    }

    public componentDidMount() {
        this.serviceProvider.dialogService = this.dialogService.current!;
        this.serviceProvider.notificationService = this.notificationService.current!;
        this.serviceProvider.storeService = this.storeService.current!;
        this.serviceProvider.svcProvider = this!;
        this.serviceProvider.themeService = this.themeService.current!;
    }
}




