import { createMuiTheme, Theme,  } from "@material-ui/core";
import orange from "@material-ui/core/colors/orange";
import pink from "@material-ui/core/colors/pink";
import blueGrey from "@material-ui/core/colors/blueGrey";
import {ThemeOptions} from "@material-ui/core/styles/createMuiTheme";
import ThemePalette from "./themePalatte.json";

const base: ThemeOptions ={
  overrides: {
    MuiButton: { // Name of the component ⚛️ / style sheet
      text: { // Name of the rule
         // Some CSS
      },
    },
    MuiIconButton: {
      root: {
        //color: "pink"
      }
    },
    MuiSwitch: {
        switchBase: {
            height: 20,
        }
    },
    MuiInputBase: {

    },
    MuiList: {
        root: {
            padding: 0,
        },
        padding: {
            paddingTop: 0,
            paddingBottom: 0,
            paddingLeft: 0,
            paddingRight: 0,
        }
    },
    MuiListSubheader: {
        root: {
            fontSize: 16,
            lineHeight: "40px",
        }
    },
    MuiListItem: {
        root: {
            paddingTop: 0,
            paddingRight: 5,
            paddingBottom: 5,
            paddingLeft: 5,
            "&:hover": {
                backgroundColor: "#383c47"
            }
        },
        gutters: {
            paddingLeft: 5,
            paddingRight: 5,
        }
    },
    MuiMenuItem: {
        root: {
            "fontSize": 12,
            "height": 15,
            "padding": 5,
            "&:hover": {
                backgroundColor: "rgba(246,168,33, 0.5) !important",
            },
            "&:hover:before": {
                backgroundColor: "rgba(246,168,33, 0.5) !important",
            },
            "&:hover:after": {
                backgroundColor: "rgba(246,168,33, 0.5) !important",
            },
            "&:active": {
                color: "black",
                backgroundColor: "rgba(246,168,33, 0.5) !important",
            },
            "&$selected": {
                backgroundColor: "rgba(246,168,33, 0.5) !important",
            }
        }
    },
    MuiSelect: {
      root: {
        color: "#DC9202",
        //backgroundColor: "#535353",
        
      },
      /*
      select: {
        color: "yellow",
        backgroundColor: "blue",

      }
      selectMenu: {
        color: "green"
      }*/
    },
  },
};

export const midnightMuiTheme: IMuiTheme = {
    themeId: ThemePalette.MidnightBlack.id,
    ...createMuiTheme({
        ...base,
        palette: {
            background: {
                default: ThemePalette.MidnightBlack.Primary2,
                paper: ThemePalette.MidnightBlack.Primary2,
            },
            primary: {
                light: ThemePalette.MidnightBlack.Contrast1, 
                main: ThemePalette.MidnightBlack.Contrast1, 
                dark: ThemePalette.MidnightBlack.Contrast1, 
                contrastText: ThemePalette.MidnightBlack.Contrast2, 
            },
            secondary: {
                light: ThemePalette.MidnightBlack.Contrast1, 
                main: ThemePalette.MidnightBlack.Contrast1, 
                dark: ThemePalette.MidnightBlack.Contrast1, 
                contrastText: ThemePalette.MidnightBlack.Primary2, 
            },
            type: "dark",
        },
    })
}

export const steelMuiTheme: IMuiTheme = {
    themeId: ThemePalette.SteelBlue.id,
    ...createMuiTheme({
        ...base,
        palette: {
            background: {
                default: ThemePalette.SteelBlue.Primary2,
                paper: ThemePalette.SteelBlue.Primary2,
            },
            primary: {
                light: ThemePalette.SteelBlue.Contrast1, 
                main: ThemePalette.SteelBlue.Contrast1, 
                dark: ThemePalette.SteelBlue.Contrast1, 
                contrastText: ThemePalette.SteelBlue.Contrast2, 
            },
            secondary: {
                light: ThemePalette.SteelBlue.Contrast1, 
                main: ThemePalette.SteelBlue.Contrast1, 
                dark: ThemePalette.SteelBlue.Contrast1, 
                contrastText: ThemePalette.SteelBlue.Primary2, 
            },
            type: "dark",
        },
    })
}

export const articMuiTheme: IMuiTheme = {
    themeId: ThemePalette.ArticWhite.id,
    ...createMuiTheme({
        ...base,
        palette: {
            background: {
                default: ThemePalette.ArticWhite.Primary1,
                paper: ThemePalette.ArticWhite.Primary1,
            },
            primary: {
                light:  ThemePalette.ArticWhite.Contrast1,
                main:  ThemePalette.ArticWhite.Contrast1,
                dark:  ThemePalette.ArticWhite.Contrast1,
                contrastText:  ThemePalette.ArticWhite.Contrast2,
            },
            secondary: {
                light: ThemePalette.ArticWhite.Cyan, 
                main: ThemePalette.ArticWhite.Cyan,
                dark: ThemePalette.ArticWhite.Cyan, 
                contrastText:  ThemePalette.ArticWhite.Contrast2,
            },
            type: "light",
        },
    })
}

/*
default: ThemePalette.MidnightBlack.Primary2,
paper: ThemePalette.MidnightBlack.Primary2,
},
primary: {
light: ThemePalette.MidnightBlack.Contrast1, 
main: ThemePalette.MidnightBlack.Contrast1, 
dark: ThemePalette.MidnightBlack.Contrast1, 
contrastText: ThemePalette.MidnightBlack.Contrast2, 
},
secondary: {
light: ThemePalette.MidnightBlack.Contrast1, 
main: ThemePalette.MidnightBlack.Contrast1, 
dark: ThemePalette.MidnightBlack.Contrast1, 
contrastText: ThemePalette.MidnightBlack.Primary2, 
},
*/
export const crystalMuiTheme: IMuiTheme = {
    themeId: ThemePalette.CrystalBlue.id,
    ...createMuiTheme({
        ...base,
        palette: {
            background: {
                default: ThemePalette.CrystalBlue.Primary1,
                paper: ThemePalette.CrystalBlue.Primary1,
            },
            primary: {
                light:  ThemePalette.CrystalBlue.Contrast1,
                main:  ThemePalette.CrystalBlue.Contrast1,
                dark:  ThemePalette.CrystalBlue.Contrast1,
                contrastText:  ThemePalette.CrystalBlue.Contrast2,
            },
            secondary: {
                light: ThemePalette.CrystalBlue.Contrast1, 
                main: ThemePalette.CrystalBlue.Contrast1, 
                dark: ThemePalette.CrystalBlue.Contrast1, 
                contrastText: ThemePalette.CrystalBlue.Primary2, 
            },
            type: "light",
        },
    })
}

export type IMuiTheme = {themeId: string} & Theme;
export const MUI_THEMES: IMuiTheme[] = [midnightMuiTheme, articMuiTheme];
 
 