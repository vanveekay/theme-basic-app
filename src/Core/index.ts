import ThemePalatte from "./ServiceProvider/themes/themePalatte.json";

export * from "./Layout/layout";
export * from "./Layout/LayoutHOC";
export * from "./ServiceProvider/serviceProvider";
export * from "./ServiceProvider/themes/muitheme";

export const MidnightBlackTheme = () => {
    return ThemePalatte.MidnightBlack
}

export const SteelBlueTheme = () => {
    return ThemePalatte.SteelBlue
}

export const CrystalBlueTheme = () => {
    return ThemePalatte.CrystalBlue
}

export const ArticWhiteTheme = () => {
    return ThemePalatte.ArticWhite
}