import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';

import DashboardIcon from '@material-ui/icons/Dashboard';
import ComputerIcon from '@material-ui/icons/Computer';
import DonutSmallIcon from '@material-ui/icons/DonutSmall';
import InfoIcon from '@material-ui/icons/Info';
import ListItem from '@material-ui/core/ListItem';

import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';

import IconButton from '@material-ui/core/IconButton';

const drawerWidth = 65;

const css = {
    listItemStyle: {
        padding: "0px",
    },
    iconButtonStyle: {
        color: "#DC9202"
    }
}

class Sidebar extends React.Component<any, any> {
    
    render(){
        return (
            <Drawer classes={{}} variant="permanent" anchor="left">
                <List style ={{ height:"100%"}}>
                    <ListItem  >
                        <Link to="/dashboard">
                            <Tooltip title="Dashboard" placement="right-end">
                                <IconButton component="span">
                                    <DashboardIcon color="primary" fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
                    <ListItem >
                        <Link to="/portfolio">
                            <Tooltip title="Portfolio Details" placement="right-end">
                                <IconButton component="span">
                                    <DonutSmallIcon color="primary" fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
                    <ListItem  >
                        <Link to="about">
                            <Tooltip title="About" placement="right-end">
                                <IconButton component="span">
                                    <InfoIcon color="primary" fontSize="large" />
                                </IconButton>
                            </Tooltip>
                        </Link>
                    </ListItem>
 
              </List>
        
          </Drawer>
        );
    }
  }

export default Sidebar;
  
/*
const styles = (theme: any) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
    backgroundColor: "#191919"
  },
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing() * 3,
  }
});
*/

/*
import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';

import DashboardIcon from '@material-ui/icons/Dashboard';
import ComputerIcon from '@material-ui/icons/Computer';
import DonutSmallIcon from '@material-ui/icons/DonutSmall';
import InfoIcon from '@material-ui/icons/Info';
import ListItem from '@material-ui/core/ListItem';

import { Link } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';
import Paper from "@material-ui/core/Paper";
import IconButton from '@material-ui/core/IconButton';

import Config from "./sidebarConfig.json";

const drawerWidth = 65;

const css = {
    listItemStyle: {
        padding: "0px",
    },
    iconButtonStyle: {
        color: "#DC9202"
    }
}

export interface IMenuItem {
    icon: JSX.Element | React.ReactChild;
    item: JSX.Element | React.ReactChild;
    link: string,
    title: string,
    placement: string
}

class Sidebar extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            activeIndex: -1,
            isOpen: false
        }
    }

    private onCloseMenu = () => {
        this.setState({ isOpen: false })
    }

    buildMainMenu = () => {
        console.log(Config)
        var mainMenuItems = Object.keys(Config)
        var subMenuItems = Object.values(Config)
        console.log(this.props)

        const onClick = (i: any) => {
            this.setState({
                activeIndex: i,
                isOpen: true
            })
        }

        return (
            <React.Fragment>
                {this.props.menuItems.map((item, i) => {
                    return (
                        <ListItem style={{ padding: 2 }} button={true}  key={i} onClick={onClick}>
                            <Link to={item.link}>
                                <Tooltip title={item.title} placement={item.placement}>
                                    <IconButton component="span">
                                        {item.icon} 
                                    </IconButton>
                                </Tooltip>
                            </Link>
                        </ListItem>
                    )
                })}
            </React.Fragment>
        )
    }

    buildSubMenu = () => {
        const overlayedItem = this.props.menuItems[this.state.activeIndex].item;
    }

    render() {
        const menuItemContainerStyle: React.CSSProperties =  { zIndex: 1450 } ;
        let drawerContentStyle: React.CSSProperties = {marginLeft: "30px"};

        return (
            <React.Fragment>
                <Drawer open={this.state.isOpen} onClose={this.onCloseMenu} style={drawerContentStyle} anchor="left">
                
                                <div style={{width: "200px"}}>
                                    hello
                                </div>
                         
                </Drawer>
                <Paper style={menuItemContainerStyle} square={true}>
                    <List>
                        {this.buildMainMenu()}
                    </List>
                </Paper>
            </React.Fragment>
        );
    }
}

export default Sidebar;

/*
 <List style={{ height: "100%" }}>
                        <ListItem  >
                            <Link to="/dashboard">
                                <Tooltip title="Dashboard" placement="right-end">
                                    <IconButton component="span">
                                        <DashboardIcon color="primary" fontSize="large" />
                                    </IconButton>
                                </Tooltip>
                            </Link>
                        </ListItem>
                        <ListItem >
                            <Link to="/portfolio">
                                <Tooltip title="Portfolio Details" placement="right-end">
                                    <IconButton component="span">
                                        <DonutSmallIcon color="primary" fontSize="large" />
                                    </IconButton>
                                </Tooltip>
                            </Link>
                        </ListItem>
                        <ListItem  >
                            <Link to="about">
                                <Tooltip title="About" placement="right-end">
                                    <IconButton component="span">
                                        <InfoIcon color="primary" fontSize="large" />
                                    </IconButton>
                                </Tooltip>
                            </Link>
                        </ListItem>

                    </List>

*/