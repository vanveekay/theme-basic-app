import * as React from "react";
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import componentJson from "./LayoutConfig/componentStaticData.json";
import {ServiceContext} from "../ServiceProvider/serviceProvider";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import * as FlexLayout from "flexlayout-react";
import { Node, TabSetNode, TabNode, DropInfo, BorderNode } from "flexlayout-react";
import Test1 from "../../Plugin/Test1";
import Test2 from "../../Plugin/Test2";
import Test3 from "../../Plugin/Test3";
import Test4 from "../../Plugin/Test4";
import { QuickItemSelector } from '../mui/quickItemSelector';
import { marketLayout } from './LayoutConfig/layoutConfig';

interface ILayoutState {
    layoutFile: string | null;
    model: FlexLayout.Model | null;
    adding: boolean;
    keyLiveData: any;
    allCurrPrice: any;
}

export class Layout extends React.Component<any, any> {
    static contextType = ServiceContext;
    items: any[] = [
        {key: "LivePrice", value: "LivePrice"},
        {key: "Portfolio", value: "Portfolio"},
        {key: "OMS", value: "OMS"},
        {key: "Terminal", value: "TerminalHOC"}
    ]
    loadingLayoutName?: string;

    constructor(props:any) {
        super(props);
        this.state = { 
            layoutFile: null, 
            model: FlexLayout.Model.fromJson(marketLayout), 
            adding: false,
            
            keyLiveData: null,
            allCurrPrice: null
        };
    
    }



    handleInputChange = (e: any) => {
        const {name, value} = e.target
        this.context.themeService.setTheme(value)
        this.context.themeService.setLayoutTheme(value);
        this.forceUpdate();
        /*
        this.setState({
            [name]: value
        })
        */
      }
      onAddTab = (componentName: string) => {
        if (this.state.model!.getMaximizedTabset() == null) {
            (this.refs.layout as FlexLayout.Layout).addTabToActiveTabSet(componentJson[componentName]);
        }
    }


    factory = (node:TabNode) => {
        var component = node.getComponent();

        switch(component) {
            case "text":
                return  <div dangerouslySetInnerHTML={{ __html: node.getConfig().text }} />;
            case "LivepriceGrid":
                return <Test1 />;
            case "PortfolioGrid":
                return <Test2 />;
            case "OrderManagement":
                return <Test3 />;
            case "TerminalHOC":
                return <Test4 />;
            default:
                return <Test1 />;
        }
    }
      
    addToLayout = (value: any) => {
        console.log(value)
        this.onAddTab(value)
    }
   
    render() {
        var onRenderTab = function (node:TabNode, renderValues:any) {
            //renderValues.content += " *";
        };

        var onRenderTabSet = function (node:(TabSetNode|BorderNode), renderValues:any) {
            //renderValues.headerContent = "-- " + renderValues.headerContent + " --";
            //renderValues.buttons.push(<img src="images/grey_ball.png"/>);
        };

        let contents: React.ReactNode = "loading ...";
        if (this.state.model !== null) {
            contents = <FlexLayout.Layout
                ref="layout"
                model={this.state.model}
                factory={this.factory}
                onRenderTab={onRenderTab}
                onRenderTabSet={onRenderTabSet} 
                />;
        }
        return (
            <React.Fragment>
                   <QuickItemSelector items={this.items} onSubmit={this.addToLayout}/>
                   <Button color="secondary" variant="contained" onClick={()=> this.onAddTab("Terminal")} style={{height: "30px", margin:"2px",backgroundColor: "#DC9202", color: "#222"}} >
                        Add Terminal
                    </Button>
                    <Button color="secondary" variant="contained" onClick={()=> this.onAddTab("LivePrice")} style={{height: "30px", margin:"2px"}} >
                        Add Live Prices
                    </Button>
                    <Button color="secondary" variant="contained" onClick={()=> this.onAddTab("Portfolio")} style={{height: "30px", margin:"2px"}} >
                        Add Portfolio
                    </Button>
                    <Button color="secondary" variant="contained" onClick={()=> this.onAddTab("OMS")} style={{height: "30px", margin:"2px"}} >
                        Add OMS
                    </Button>
                <div className="toolbar">
                    <Button color="secondary" variant="outlined" onClick={()=>{
                        this.context.themeService.setTheme("ArticWhite")
                        this.context.themeService.setLayoutTheme("ArticWhite");
                        //setLayoutTheme();
                        this.forceUpdate()
 
                    }}>Set Theme To Light</Button>
                    <Button color="secondary" variant="outlined" onClick={()=>console.log(this.context.themeService.getAgTheme())}>Get Theme</Button>
                    <div style={{height: "30px"}}>
                    <Select
                                value={''} 
                                onChange={this.handleInputChange}
                                displayEmpty
                                name='orderType'
                                style={{
                                    backgroundColor: "rgb(64, 64, 64, 0.5)",
                                    paddingLeft: "5px",
                                    width: "100%"
                                }}
                                >
                                <MenuItem value="ArticWhite">Artic White</MenuItem>
                                <MenuItem value="SteelBlue">Steel Blue</MenuItem>
                                <MenuItem value="CrystalBlue">Crystal Blue</MenuItem>
                                <MenuItem value="MidnightBlack">Midnight Black</MenuItem>
                            </Select>
                            </div>
                    <Typography style={{maxWidth: "50%", position: "absolute", right: "100px", top: "5px"}} color="primary" align="right" variant="body1">
                        Note: AlphaVantage API max calls 5 per min. If error, please wait and try again.
                    </Typography>
                </div>
                <div className="contents">
                    {contents}
                </div>
            </React.Fragment>
        );
    }

}
