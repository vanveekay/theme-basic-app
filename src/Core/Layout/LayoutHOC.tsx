import React from 'react';
import {Layout} from './layout';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const message = 'Drag & Drop to build your dashboard from the buttons above';

const notificationOptions = {
        variant: 'default',
        autoHideDuration: 2000,
        disableWindowBlurListener: true,
        anchorOrigin: {
            vertical: 'bottom',
            horizontal: 'right',
        }
}
class LayoutHOC extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.showNotification();
    }

    styleNotification = (message: string) => {
        return (
            <React.Fragment>
                <Card style={{backgroundColor: "rgb(220, 146, 2)", padding: 0}}>
                    <CardContent>
                        <Typography style={{color: "#222"}} gutterBottom>
                            Notification
                        </Typography>
                        <Typography style={{color: "#222",minWidth: "300px", maxWidth: "300px"}} variant="body1">
                            {message}
                        </Typography>
                    </CardContent>
                </Card>
            </React.Fragment>
        )
    }
    /**
     * 
                    <CardActions>
                        <Button size="small">Learn More</Button>
                    </CardActions
     */
    showNotification = () => {
        //this.props.enqueueSnackbar(this.styleNotification("Welcome !"), notificationOptions);
        //this.props.enqueueSnackbar(this.styleNotification("Drag & Drop to build your dashboard from the buttons above"), notificationOptions);
    }

    render(){
        console.log('layouthoc');
        return (
            <React.Fragment>
                <Layout />
            </React.Fragment>
        );
    }
}

export default LayoutHOC