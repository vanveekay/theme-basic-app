import { withStyles } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import Select from "react-select";
import {IReactSelectProps} from "./underlinedReactSelect";
import { createStyles, emphasize, makeStyles, useTheme, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      height: 250,
    },
    input: {
      display: 'flex',
      padding: 0,
      height: 'auto',
    },
    valueContainer: {
      display: 'flex',
      flexWrap: 'wrap',
      flex: 1,
      alignItems: 'center',
      overflow: 'hidden',
    },
    chip: {
      margin: theme.spacing(0.5, 0.25),
    },
    chipFocused: {
      backgroundColor: emphasize(
        theme.palette.type === 'light' ? theme.palette.grey[300] : theme.palette.grey[700],
        0.08,
      ),
    },
    noOptionsMessage: {
      padding: theme.spacing(1, 2),
    },
    singleValue: {
      fontSize: 16,
    },
    placeholder: {
      position: 'absolute',
      left: 2,
      bottom: 6,
      fontSize: 16,
    },
    paper: {
      position: 'absolute',
      zIndex: 1,
      marginTop: theme.spacing(1),
      left: 0,
      right: 0,
    },
    divider: {
      height: theme.spacing(2),
    },
  }),
);



const styles = (theme: Theme) => ({
    chip: {
        margin: `${theme.spacing() /2}px ${theme.spacing() / 4}px`,
    },
    chipFocused: {
        backgroundColor: "white"
    },
    input: {
        color: "white",
        display: "flex",
        padding: 0,
        paddingLeft: "13px",
        height: "23px !important",
    },
    noOptionsMessage: {
        padding: `${theme.spacing()}px ${theme.spacing() * 2}px`
    },
    paper: {
        left: 0,
        marginTop: "2px",
        position: "absolute",
        right: 0,
        zIndex:1,
        backgroundColor: "#17355D"
    },
    placeholder: {
        color: "white",
        fontSize: 14,
        left: 0,
        paddingLeft: "15px",
        paddingTop: "3px",
        position: "absolute",
    },
    root: {
        backgroundColor: "#0F223E",
        border: "1px solid grey",
        color: "white",
        flexGrow: 1,
    },
    singleValue: {
        color: "white",
        fontSize: 14,
        paddingLeft: "0px",
        paddingBottom: "4px",
    },
    valueContainer: {
        alignItems: "center",
        color: "white",
        display: "flex",
        flex: 1,
        flexWrap: "wrap",
        overflow: "hidden",
        paddingLeft: "0px"
    }
});

function noOptionsMessage(props: any) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function inputComponent({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

function Control(props: any) {
    return (
        <TextField 
            fullWidth={true}
            inputProps={{
                disableUnderline: true,
                inputComponent,
                inputProps: {
                    children: props.children,
                    className: props.selectProps.classes.input,
                    inputRef: props.inputRef,
                    ...props.innerProps,
                }
            }}
        {...props.selectedProps.textFieldProps}
        />
    )
}

function Option (props: any) {
    return (
        <MenuItem  
            buttonRef={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontSize: "14px",
                fontWeight: props.isSelected ? 500 : 400,
                height: "24px",
                padding: "4px",
                paddingLeft: "14px",
                color: "white"
            }}
            {...props.innerProps}
        >
        {props.children}
        </MenuItem>
    )
}

function Placeholder(props: any) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function SingleValue(props: any) {
    return (
        <Typography
            className={props.selectProps.classes.singleValue}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function ValueContainer(props: any) {
    return (
        <div className={props.selectProps.classes.valueContainer}>
            {props.children}
        </div>
    )
}

function Menu(props: any) {
    return (
        <Paper
            square={true}
            className ={props.selectProps.classes.paper}
            {...props.innerProps}
        >
            {props.children}
        </Paper>
    )
}

const components = {
    Control,
    Menu,
    // MultiValue,
    noOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
}

export class ReactSelect extends React.Component<IReactSelectProps> {
    constructor(props: IReactSelectProps) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    public render() {
        //const {classes} = this.props;
        const classes = useStyles({});
        const theme = useTheme();

        const selectStyles = {
            input: (base) => ({
                ...base,
                color: "white",
                "& input": {
                    font: "inherit",
                }
            })
        }

        return (
            <div className={classes.root}>
                <Select
                    autoFocus={true}
                    classes={classes}
                    styles={selectStyles}
                    options={this.props.options}
                    components={components}
                    onChange={this.onChange}
                    placeholder={this.props.selection || "Search..."}
                    isClearable={false}
                />
            </div>
        )
    }

    private onChange(event: any) {
        if (event) {
            const {label, value} = event;
            this.props.onChange({label, value});
        }
    }
}

export default withStyles(styles as any, {withTheme: true})(
    ReactSelect,
)