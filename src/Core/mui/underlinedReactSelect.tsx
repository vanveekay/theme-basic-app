import {Theme, withStyles } from "@material-ui/core";
import MenuItem from "@material-ui/core/MenuItem";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import * as React from "react";
import Select from "react-select";
import {emphasize} from "@material-ui/core/styles/colorManipulator";

const styles = (theme: Theme) => ({
    chip: {
        margin: `${theme.spacing() /2}px ${theme.spacing() / 4}px`,
    },
    chipFocused: {
        backgroundColor: emphasize(
            theme.palette.type === "light"
                ? theme.palette.grey[300]
                : theme.palette.grey[700]
        )
    },
    divider: {
        height: theme.spacing() *2
    },
    input: {
        display: "flex",
        padding: 0,
    },
    noOptionsMessage: {
        padding: `${theme.spacing()}px ${theme.spacing() * 2}px`
    },
    paper: {
        left: 0,
        marginTop: theme.spacing(),
        position: "absolute",
        right: 0,
        zIndex:1,
    },
    placeholder: {
        fontSize: 14,
        left: 0,
        paddingLeft: "11px",
        position: "absolute",
    },
    root: {
        flexGrow: 1,
    },
    singleValue: {
        fontSize: 14,
        paddingLeft: "11px",
    },
    valueContainer: {
        alignItems: "center",
        display: "flex",
        flex: 1,
        flexWrap: "wrap",
        overflow: "hidden",
    }
});

function noOptionsMessage(props: any) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.noOptionsMessage}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function inputComponent({inputRef, ...props}) {
    return <div ref={inputRef} {...props} />;
}

function Control(props: any) {
    return (
        <TextField 
            fullWidth={true}
            inputProps={{
                disableUnderline: true,
                inputComponent,
                inputProps: {
                    children: props.children,
                    className: props.selectProps.classes.input,
                    inputRef: props.inputRef,
                    ...props.innerProps,
                }
            }}
        {...props.selectedProps.textFieldProps}
        />
    )
}

function Option (props: any) {
    return (
        <MenuItem  
            buttonRef={props.innerRef}
            selected={props.isFocused}
            component="div"
            style={{
                fontSize: "14px",
                fontWeight: props.isSelected ? 500 : 400,
                height: "24px",
                padding: "4px",
                paddingLeft: "14px",
                color: "white"
            }}
            {...props.innerProps}
        >
        {props.children}
        </MenuItem>
    )
}

function Placeholder(props: any) {
    return (
        <Typography
            color="textSecondary"
            className={props.selectProps.classes.placeholder}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function SingleValue(props: any) {
    return (
        <Typography
            className={props.selectProps.classes.singleValue}
            {...props.innerProps}
        >
            {props.children}
        </Typography>
    )
}

function ValueContainer(props: any) {
    return (
        <div className={props.selectProps.classes.valueContainer}>
            {props.children}
        </div>
    )
}

function Menu(props: any) {
    return (
        <Paper
            square={true}
            className ={props.selectProps.classes.paper}
            {...props.innerProps}
        >
            {props.children}
        </Paper>
    )
}

const components = {
    Control,
    Menu,
    // MultiValue,
    noOptionsMessage,
    Option,
    Placeholder,
    SingleValue,
    ValueContainer,
}

export interface IReactSelectProps {
    selection?: string;
    theme?: Theme;
    classes?: any;
    options: Array<{label: string, value: string}>;
    onChange: (result: {label: string, value: string}) => void;
}

export class UnderlinedReactSelect extends React.Component<IReactSelectProps> {
    constructor(props: IReactSelectProps) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    public render() {
        const {classes, theme} = this.props;

        const selectStyles = {
            input: (base) => ({
                ...base,
                color: "white",
                "& input": {
                    font: "inherit",
                }
            })
        }

        return (
            <div className={classes.root}>
                <Select
                    autoFocus={true}
                    classes={classes}
                    styles={selectStyles}
                    options={this.props.options}
                    components={components}
                    onChange={this.onChange}
                    placeholder={this.props.selection || "Search..."}
                    isClearable={false}
                />
            </div>
        )
    }

    private onChange(event: any) {
        if (event) {
            const {label, value} = event;
            this.props.onChange({label, value});
        }
    }
}

export default withStyles(styles as any, {withTheme: true})(
    UnderlinedReactSelect,
)

// export {ActionMeta} from "react-select/lib/types";