import Modal from "@material-ui/core/Modal";
import * as React from "react";
import {UnderlinedSearch} from "./underlinedSearch";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

export interface IModalSearchItem {
    label: any;
    value: any;
}

export interface ISearchItemSelectorProps {
    selection?: string;
    items: any | IModalSearchItem[];
    padding?: string;
    leftPadding?: string;
    rightPadding?: string;
    bottomPadding?: string;
    topPadding?: string;
    showSearchIcon?: boolean;
    onSubmit: (value: any | IModalSearchItem) => void;
}

export interface IModalSearchProps {
    items: any[];
    padding?: string;
    leftPadding?: string;
    rightPadding?: string;
    bottomPadding?: string;
    topPadding?: string;
    showSearchIcon?: boolean;
    onSubmit: (value: any | IModalSearchItem) => void;
}

interface IModalSearchState {
    isOpen: boolean;
}

export class ModalSearch extends React.Component< IModalSearchProps, IModalSearchState> {

    constructor(props: IModalSearchProps) {
        super(props);

        this.state = { isOpen: false}
        window.addEventListener("keydown", this.keyPress);
    }

    public componentWillUnmount() {
        window.removeEventListener("keydown", this.keyPress);
    }

    public render() {
        var items: any =  this.props.items ? this.props.items : []
        
        return (
            <Modal  
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.isOpen}
                onClose={this.onClose}
            >
                <div style={{left: "50%", position: "absolute", top:"50%", transform: `translate(-50%,-50%)`, width: "400px"}}>
                    <Select
                        value={''} 
                        onChange={this.onSubmit}
                        displayEmpty
                        name='orderType'
                        style={{
                            backgroundColor: "rgb(64, 64, 64, 0.5)",
                            paddingLeft: "5px",
                            width: "100%"
                        }}
                        >
                            {items.map((item, i)=> {
                            return (
                                <MenuItem key={i} value={item.key}>{item.value}</MenuItem>
                            )
                        })}
                    </Select>
                </div>
            </Modal>
        )
    }

    private labelValueCheck = () => {
        const data: any[] = [];
        if (!this.props.items[0].value) {
            this.props.items.map((_) => {
                data.push({label: _, value: _})
            })
            return data;
        }
        return this.props.items;
    }

    private onSubmit = (e: any) => {
        const {name, value} = e.target
        this.props.onSubmit(value);
        this.setState({isOpen: false})
    }
    
    private onClose = () => {
        this.setState({isOpen: false})
    }

    private keyPress = (event: KeyboardEvent) => {
        if (event.ctrlKey && event.code === "KeyF") {
            event.preventDefault();
            this.setState({isOpen: true})
        } else if (event.code === "Escape") {
            this.setState({isOpen: false})
        }
    }
}