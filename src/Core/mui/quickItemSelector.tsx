import { IModalSearchItem, ModalSearch } from "./modalSearch";
import ProviderHOC from "../../ProviderHOC";
import * as React from "react";


interface IItemSelectorState {
    value: IModalSearchItem | null;
}

export class QuickItemSelector extends React.Component<any, IItemSelectorState> {
    
    constructor(props: any) {
        super(props);
        this.state = {
            value: null
        }
    }
    public render() {
        return (
            <div style={{
                alignItems: "center",
                display: "flex"
            }}
            >
                <div style={{ flex: "1 1 0" }}>
                    <ModalSearch items={this.props.items} onSubmit={this.onChange} padding="8px" />
                </div>
            </div>
        )
    }

    private onChange = (value: any) => {
        console.log(value)
        this.props.onSubmit(value);
    }

}
export default QuickItemSelector

    
