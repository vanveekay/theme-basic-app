import SearchIcon from "@material-ui/icons/Search";
import * as React from "react";
import {ISearchItemSelectorProps} from "./modalSearch";
import Reactselect, { ReactSelect } from "./reactSelect";

export class StandardSearch extends React.Component< ISearchItemSelectorProps> {
    constructor(props: ISearchItemSelectorProps) {
        super(props);

        this.onChange= this.onChange.bind(this);
    }

    public render() {
        return (
            <div style ={{
                alignItems: "center",
                backgroundColor: "#2A2D35",
                borderRadius: "6px",
                display: "flex",
                padding: this.props.padding || "0px",
                paddingBottom: this.props.bottomPadding || this.props.padding || "0px",
                paddingLeft: this.props.leftPadding || this.props.padding || "0px",
                paddingRight: this.props.rightPadding || this.props.padding || "0px",
                paddingTop: this.props.topPadding || this.props.padding || "0px",
            }}
            >
                {this.props.showSearchIcon &&
                    <SearchIcon color={"secondary"} />
                }

                standard
            </div>
        )
    }

    private onChange(result: {label:string, value: string}) {
        this.props.onSubmit(result.value);
    }
}