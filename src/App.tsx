import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Layout} from "./Core/Layout/layout";
import ProviderHOC from "./ProviderHOC";
import { Route, BrowserRouter as Router, Link } from 'react-router-dom';
import Sidebar from "./Core/Sidebar";
import Test1 from './Plugin/Test1';
import Test2 from './Plugin/Test2';

const App: React.FC = () => {
  return (
    <ProviderHOC>
   <Router>
          <div style={{width: "65px"}}>
      <Sidebar />
      </div>
          <div style={{width: `calc(100% - 65px)`}}>
          <Route exact path="/" component={Test1} />
          <Route path="/dashboard" component={Layout} />
          <Route path="/terminal" component={Test2} />
          </div>
      </Router>
    </ProviderHOC>
  );
}

export default App;
