import {ServiceProvider } from "./Core/ServiceProvider/serviceProvider";
import * as React from "react";

const ProviderHOC: React.FunctionComponent = (props) => {
    return (
        <ServiceProvider>
            {props.children}
        </ServiceProvider>
    )
}

export default ProviderHOC;